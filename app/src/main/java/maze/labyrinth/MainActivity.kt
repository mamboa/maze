package maze.labyrinth

/**
 * Project: Labyrinth
 * Package: maze.labyrinth.MainActivity
 * Created by mamboa on 2016-05-11.
 * Description:
 * - Represents the activity displaying the main view
 */

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

import maze.labyrinth.Options.OptionActivity
import maze.labyrinth.Partie.Game
import maze.labyrinth.Tableau.GridInterface

class MainActivity : AppCompatActivity(), GridInterface {
    override var currentColor: Int?
        get() = game?.currentColor
        set(value) {}

    private var game: Game? = null


    private var gameNumber = 1
    private val preferences: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(this)
    }

    private val customHandler = Handler()
    internal var timeInMilliseconds = 0L
    internal var timeSwapBuff = 0L
    internal var updatedTime = 0L
    private var startTime = 0L
    var isGameStarted = false
        private set

    companion object {

        private const val DFSSolver = 2
        private const val BFSSolver = 1
        private const val maximumSize = 30
        private const val minimumSize = 3
        private const val defaultSize = 5

        val PREF_FILE_NAME = "pref_general"
    }


    private val updateTimerThread = object : Runnable {
        override fun run() {
            timeInMilliseconds = SystemClock.uptimeMillis() - startTime
            updatedTime = timeSwapBuff + timeInMilliseconds

            var secs = (updatedTime / 1000).toInt()
            val mins = secs / 60
            secs %= 60
            val milliseconds = (updatedTime % 1000).toInt()
            stopWatchTextView.text = ("" + mins + ":"
                    + String.format("%02d", secs) + ":"
                    + String.format("%02d", milliseconds))
            customHandler.postDelayed(this, 0)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // if we are asked to generate a grid with a different size, we reload the whole intent
        val extras = intent.extras
        val gridSize = extras?.getInt("Id", 5) ?: defaultSize

        gridView.gridInterface = this
        game = Game(gridView)
        game?.initializeGameWithSize(gridSize, preferences)
        updateGridSize()
        updateSquares()

        stopWatchTextView.text = ("" + 0 + ":"
                + String.format("%02d", 0) + ":"
                + String.format("%02d", 0))

        genererButton.setOnClickListener{ resetGame() }
        sizeButton.setOnClickListener{
            levelSettingDialog()
            updateGridSize()
            updateSquares()
        }

        solutionButton.setOnClickListener{
            val dot1 = game?.dot1
            val dot2 = game?.dot2
            if (dot1 != null && dot2 != null) {
                when (Integer.parseInt(preferences.getString(getString(R.string.defaultSolverTag), DFSSolver.toString()) ?:  DFSSolver.toString())) {
                    BFSSolver -> gridView.solveBFS(gridView.getSquareIdGivenADot(dot1),
                            gridView.getSquareIdGivenADot(dot2))
                    DFSSolver -> gridView.solveDFS(gridView.getSquareIdGivenADot(dot1),
                            gridView.getSquareIdGivenADot(dot2))
                    else -> gridView.solveDFS(gridView.getSquareIdGivenADot(dot1),
                            gridView.getSquareIdGivenADot(dot2))
                }

                updateGridSize()
                updateSquares()
                pauseStopWatch()
                isGameStarted = false
            }
        }
        titleTextView.text = getString(R.string.random_game, gameNumber)

        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayUseLogoEnabled(true)
        supportActionBar?.setLogo(R.mipmap.ic_launcher)
        supportActionBar?.setDisplayShowTitleEnabled(true)
    }

    public override fun onStart() {
        super.onStart()
        updateGridSize()
        updateSquares()
    }

    override fun onStop() {
        customHandler.removeCallbacks(updateTimerThread)
        super.onStop()
    }

    override fun onResume() {
        super.onResume()

      //bad implementation as there is a repetition
        val temp = Color.parseColor("#0070C0")
        val color = preferences.getInt("color", temp)
        game?.currentColor = color

        game?.let{
            gridView.invalidate()
            gridView.applyColorByItsIndex(it.currentColor)
            gridView.loadAllColors()
            gridView.invalidate() //refresh the view to display the changes
        }
        if (isGameStarted)
            startStopWatch()
    }

    override fun onPause() {
        super.onPause()
        if (isGameStarted)
            pauseStopWatch()
    }

    /**
     * updates the state of the game after any action of the player
     */
    override fun update() {
        updateGridSize()
        updateSquares()

        if (gridView.ifGameIsOver()) {
            //resetStopWatch()
            pauseStopWatch()
            displayVictoryDialog()
        }
    }

    /**
     * leave a game
     */
    override fun onBackPressed() {
        val dialog = AlertDialog.Builder(this)
                .setTitle(R.string.leave)
                .setMessage(R.string.leave_question)
                .setPositiveButton(android.R.string.yes) { _, _ ->
                    finish()
                }
                .setNegativeButton(R.string.cancel) { _, _ ->
                    // Do nothing
                }
        dialog.show()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_color_picker_dialog -> {
                if (isGameStarted)
                    pauseStopWatch()
                startActivity(Intent(this@MainActivity, OptionActivity::class.java))
                return true
            }
            /*   case R.id.menu_about:
                new AboutDialog(this).show();
                return true;*/
            android.R.id.home -> {
                startActivity(Intent(this@MainActivity, MainActivity::class.java))
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }


    fun updateGridSize() {
        val gridSize =  game?.gridSize ?: " "
        levelTextView.text = getString(R.string.grid_size, gridSize,  gridSize)
    }

    /**
     * updateSquares
     * update the number of filled squares
     */
    override fun updateSquares() {
        numberOfSquaresTextView.text = getString(R.string.filled_squares,  gridView.numberOfFilledSquares())
    }

    /**
     * displayVictoryDialog
     * dialog box to be displayed in case of victory
     */
    private fun displayVictoryDialog() {
        val dialog = AlertDialog.Builder(this)
                .setTitle(R.string.congratulations)
                .setNegativeButton(R.string.newgame) { dialog, which -> resetGame() }
        dialog.show()
    }


    /**
     * reset
     * reset the game field (dots, and walls)
     */
    private fun resetGame() {
        game?.let {
            resetStopWatch()
            gridView.reset()
            game?.initializeGameWithSize(preferences = preferences)

            updateGridSize()
            updateSquares()

            titleTextView.text = getString(R.string.random_game,  ++gameNumber)
            stopWatchTextView.text = ("" + 0 + ":"
                    + String.format("%02d", 0) + ":"
                    + String.format("%02d", 0))
            isGameStarted = false
        }
    }

    /**
     * displays the dialog box when btnLevel_ is clicked
     */
    internal fun levelSettingDialog() {
        pauseStopWatch()
        val builder = AlertDialog.Builder(this)
        val edittext = EditText(this.applicationContext)
        edittext.setTextColor(Color.BLACK)

        builder.setMessage(getString(R.string.type_number, minimumSize, maximumSize))
        builder.setTitle(getString(R.string.grid_size_simple))
        builder.setView(edittext)
        builder.setPositiveButton(R.string.load) { dialog, whichButton ->
            val value = edittext.text.toString()
            dialog.cancel()

            // TODO: Move this matcher in a parameter
            if (value.matches("^[0-9]+$".toRegex())) {
                val newGridSize = Integer.parseInt(value)
                if (newGridSize in minimumSize..maximumSize) {
                    game?.gridSize = newGridSize
                    loadNewGridGame()
                    Toast.makeText(applicationContext, R.string.loading, Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(applicationContext, getString(R.string.type_number, minimumSize, maximumSize), Toast.LENGTH_SHORT).show()
                    startStopWatch()
                }
            } else {
                Toast.makeText(applicationContext, getString(R.string.digits_only), Toast.LENGTH_SHORT).show()
                startStopWatch()
            }
        }
        builder.setNegativeButton(getString(R.string.cancel), DialogInterface.OnClickListener { dialog, whichButton ->
            dialog.cancel()
            return@OnClickListener
        })
        builder.setOnCancelListener { startStopWatch() }
        val alert = builder.create()
        alert.show()
    }

    /**
     * loadNewGridGame
     * load a grid when the dimensions are different
     */
    private fun loadNewGridGame() {
        val intent = this.intent
        intent.putExtra("Id", game?.gridSize)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_ANIMATION)
        this.overridePendingTransition(0, 0)
        this.finish()
        this.overridePendingTransition(0, 0)
        startActivity(intent)
    }


    /**
     * Stop watch logic
     */
    override fun startStopWatch() {
        startTime = SystemClock.uptimeMillis()
        customHandler.postDelayed(updateTimerThread, 0)
    }

    private fun resetStopWatch() {
        timeInMilliseconds = 0L
        timeSwapBuff = 0L
        updatedTime = 0L
        startTime = 0L
        customHandler.removeCallbacks(updateTimerThread)
    }

    private fun pauseStopWatch() {
        timeSwapBuff += timeInMilliseconds
        customHandler.removeCallbacks(updateTimerThread)
    }

    override fun setStartedGame(gameStarted: Boolean) {
        this.isGameStarted = gameStarted
    }

}
