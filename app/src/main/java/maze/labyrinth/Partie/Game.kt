package maze.labyrinth.Partie


import android.content.SharedPreferences
import android.graphics.Color
import maze.labyrinth.Tableau.Dot
import maze.labyrinth.Tableau.GridView
import maze.labyrinth.Tableau.Square
import java.util.*
import kotlin.math.abs

/**
 * This class defines the different levels. Only when this class is installed that the game
 * can start
 * Created by TP1 on 01/02/2016
 */
class Game(val gridView: GridView) {

    var numberOfColors = 0
    var currentColor = 0
        set(value) {
            gridView.addAColor(value)
            val colorID = gridView.getColorID(value)

            field = if (colorID == -1) {
                0
            } else {
                colorID
            }
            dot1?.colorIndex = colorID
            dot2?.colorIndex = colorID
        }
    var gridSize = 0
    val dots =  ArrayList<Dot>()

    var gameID = 0

    internal var dot1: Dot? = null
    internal var dot2: Dot? = null

    var generator = Random()

    constructor(id: Int, size: Int, dots: ArrayList<Dot>, gridView: GridView) : this(gridView) {
        gameID = id
        gridSize = size
        dots.clear()
        this.dots.addAll(dots)
    }

    init {
        this.dots.addAll(dots)
        numberOfColors = this.dots.size / 2
    }

    /**
     *
     * @param gridSize
     * @return return a game (full of parameters)
     */
    fun initializeGameWithSize(gridSize: Int = -1, preferences: SharedPreferences? = null)/*: Game*/ {
        // load the color from preferences and apply it
        val tempColor = Color.parseColor("#0070C0")
        val color = preferences?.getInt("color", tempColor) ?: tempColor
        currentColor = color

        if(gridSize > -1) {
            this.gridSize = gridSize
        }

        //avoid  two points to be in the same square
        val tab = IntArray(4)
        generateDots(tab)

        // if the dots already exist just change their positions in the grid

        if (dot1 != null && dot2 != null) {
            dot1?.setDot(tab[0], tab[1], currentColor)
            dot2?.setDot(tab[2], tab[3], currentColor)
        } else {
            dot1 = Dot(tab[0], tab[1], currentColor)
            dot2 = Dot(tab[2], tab[3], currentColor)
        }
        dots.clear()
        dot1?.run { dots.add(this) }
        dot2?.run { dots.add(this) }

        gameID++
        numberOfColors = this.dots.size / 2

        gridView.initializeGame(this)
        gridView.invalidate() //refresh the view to display the changes
    }

    /**
     * generate two dots and make sure they are not in the same square
     * @param tab
     */
    fun generateDots(tab: IntArray) {
        var room1 = 0
        var room2 = 0
        var areDirectHorizontalNeighbors = false
        var areDirectVerticalNeighbors = false
        var areInTheSameRoom = false
        do {
            for (i in tab.indices) {
                tab[i] = randomSize(gridSize)
            }
            room1 = Square(tab[0], tab[1]).getSquareIdGivenDimensions(gridSize)
            room2 = Square(tab[2], tab[3]).getSquareIdGivenDimensions(gridSize)

            areDirectHorizontalNeighbors = abs(room1 - room2) == gridSize
            areDirectVerticalNeighbors = abs(room1 - room2) < gridSize - 1
            areInTheSameRoom = room1 - room2 == 0
        } while (areInTheSameRoom || areDirectVerticalNeighbors || areDirectHorizontalNeighbors)
    }

    /**
     * returns a random integer between 0 and size -1
     * @param size
     * @return
     */
    fun randomSize(size: Int): Int {
        return if (size == 0) 0 else generator.nextInt(size)
    }
}
