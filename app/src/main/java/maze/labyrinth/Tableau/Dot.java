package maze.labyrinth.Tableau;

/**
 * Project: Labyrinth
 * Package: maze.labyrinth.Tableau.Dot
 * Created by mamboa on 2016-05-11.
 * Description:
 * This class defines a dot represented by a circle in a square
 */
public class Dot {

    //square in whom the dot is drawm
    private Square square_;
    //color index of the dot
    private int colorIndex_;

    /**
     * constructor
     * @param aSquare
     * @param colorIndex
     */
    public Dot(Square aSquare, int colorIndex) {
        square_ = aSquare;
        colorIndex_ = colorIndex;
    }

    /**
     * Constructor
     * @param x
     * @param y
     * @param colorIndex
     */
    public Dot(int x, int y, int colorIndex) {
        square_ = new Square(x, y);
        colorIndex_ = colorIndex;
    }

    public void setDot(int x, int y, int colorIndex){
        square_.setColumn(x);
        square_.setLine(y);
        colorIndex_ = colorIndex;
    }

    public Square getSquare() {
        return square_;
    }
    public int getColorIndex() {
        return colorIndex_;
    }
    public void setColorIndex(int couleur){ colorIndex_ = couleur;}
}
