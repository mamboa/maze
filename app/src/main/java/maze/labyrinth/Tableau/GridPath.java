package maze.labyrinth.Tableau;

import java.util.ArrayList;
import java.util.List;

/**
 /**
 * Project: Labyrinth
 * Package: maze.labyrinth.Tableau.GridPath
 * Created by mamboa on 2016-05-11.
 * Description:
 *      This class describes the a path of squares displayed on the grid
 */

public class GridPath {

    private ArrayList<Square> gridPath;
    private int colorIndex;
    private boolean over;

    /**
     * constructor
     * @param colorIndex :  index of the color
     */
    public GridPath(int colorIndex) {
        this.colorIndex = colorIndex;
        gridPath = new ArrayList<Square>();
        over = false;
    }

    public int getColorIndex() {
        return colorIndex;
    }

    /**
     * resetPath()
     * erase the grid path
     */
    public void resetPath() {
        gridPath.clear();
    }

    public void addSquare(Square uneCase) {
        gridPath.add(uneCase);
    }

    /**
     * getGridPath()
     * returns the path formed by a list of squares
     */
    public List<Square> getGridPath() {
        return gridPath;
    }

    public Square getTheFirstSquare() {
        if(!ifEmpty()) {
            return gridPath.get(0);
        }
        return null;
    }

    public Square getTheLastSquare() {
        if(!ifEmpty()) {
            return gridPath.get(gridPath.size() - 1);
        }
        return  null;
    }

    public boolean ifEmpty() {
        return gridPath.isEmpty();
    }

    /**
     * removeASquare()
     * removes from the grid path, all the other squares
     * from the last square to the one passed as parameter
     * @param aSquare: the square to be removed
     */
    public void removeASquare(Square aSquare) {
        int indice = gridPath.indexOf(aSquare);
        if(indice >= 0) {
            for (int i = gridPath.size()-1; i > indice; --i) {
                gridPath.remove(i);
            }
        }
    }

    public boolean contains(Square aSquare) {
        return getGridPath().contains(aSquare);
    }

    public boolean isOver(){    return over; }
    public void setIsOver(boolean over){ this.over = over; }
    public void setColorIndex(int color){ colorIndex = color;}
}
