package maze.labyrinth.Tableau;

/**
 * Project: Labyrinth
 * Package: maze.labyrinth.Grille.Wall
 * Created by mamboa on 2016-05-12.
 * Description:
 *  A wall is the separation between two adjacent squares
 *  Plus we need to know if that separation is horizontal of vertical
 */
public class Wall {
    public int square1_;
    public int square2_;
    public boolean horizontal_;
    public Wall(int sqr1, int sqr2, boolean horizontal){
        this.square1_ = sqr1;
        this.square2_ = sqr2;
        this.horizontal_ = horizontal;
    }
}
